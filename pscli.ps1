Param (
          [Parameter(Mandatory=$false)]  [String]$nucName = "nucbro", # default
          [Parameter(Mandatory=$false)]  [String]$nucRG = "nuc", #default
          [Parameter(Mandatory=$false)]  [String]$defaultLocation = "North Europe" #default
)
# Connect-AzAccount -Subscription <sub-id>

$var = $MyInvocation.MyCommand.path 
$path = Split-Path $var -Parent

$templateFile = $path + "\arm.json"
$parameterFile = $path + "\arm.parameters.json"
$userId = (Get-AzADUser -StartsWith dany).id
$principleName = (Get-AzADUser -StartsWith dany).UserPrincipalName

# $keyVaultName = $nucRG + "vault"
# $keyVaultKey = $nucRG + "key"
# $secretValue = Read-Host -Prompt "Enter the virtual machine administrator password" -AsSecureString

New-AzResourceGroup `
  -Name $nucRG `
  -Location $defaultLocation

# New-AzKeyVault -Name $keyVaultName -ResourceGroupName $nucRG -Location $defaultLocation
# Set-AzKeyVaultAccessPolicy -VaultName $keyVaultName -UserPrincipalName $principleName -PermissionsToSecrets get,set,list,delete
# $secret = Set-AzKeyVaultSecret -VaultName $keyVaultName -Name $keyVaultKey -SecretValue $secretvalue

New-AzResourceGroupDeployment `
  -Name devenvironment `
  -ResourceGroupName $nucRG `
  -TemplateFile $templateFile `
  -TemplateParameterFile $parameterFile -verbose